# ERC20 Token - Deployment Guide

- Install truffle
```sh
npm i truffle -g
```

- Clone the repo into a folder `erc20_token`

- Run `cd erc20_token`

- Install the dependencies - `npm i`

- Create a file `.env` and add the following info
```
INFURA_KEY = <INFURA_KEY>
PRIVATE_KEY = <DEPLOYMENT_KEY>
```

- Deploy the contracts - `truffle migrate --network mainnet`

